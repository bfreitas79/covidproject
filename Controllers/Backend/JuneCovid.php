<?php

use JuneCovid\Models\Covid;

class Shopware_Controllers_Backend_JuneCovid extends \Shopware_Controllers_Backend_Application

{
    //The property $model must be the complete name of ../JuneCovid/Models/<JuneCovid>.php
    protected $model = Covid::class;
    //The property $alias will be the query alias used in every query with the root model ($model property).
    protected $alias = 'covid';

}
