{*content-main--inner*}
{extends file="parent:frontend/index/index.tpl"}
{block name="frontend_index_content"}
    <!DOCTYPE html>
    <html>
    <head>
        <style>

            h1 {
                text-align: left;
                text-transform: uppercase;
                color: #ff4d39;
            }

            p.{
                text-align: justify;
                letter-spacing: 3px;
            }

            a {
                text-decoration: none;
                color: #008CBA;
            }
        </style>
    </head>
    <body>

    <div>
        <h1>Stay alert</h1>
        <p>We can all help control the virus if we all stay alert. This means you must:

            stay at home as much as possible
            work from home if you can
            limit contact with other people
            keep your distance if you go out (2 metres apart where possible)
            wash your hands regularly
            Do not leave home if you or anyone in your household has symptoms.
            <a target="_blank" href="https://www.gov.uk/government/publications/coronavirus-outbreak-faqs-what-you-can-and-cant-do/coronavirus-outbreak-faqs-what-you-can-and-cant-do">"Read more about what you can and cannot do"</a></p>
    </div>

    </body>
    </html>


{/block}
