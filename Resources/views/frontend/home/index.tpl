{*content-main--inner*}
{extends file="parent:frontend/index/index.tpl"}

{block name='frontend_index_html'}
{*********** BEGINNING OF CORONAVIRUS BANNER **************}


<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .alert {
            padding: 20px;
            background-color: #f44336;
            color: white;
        }

        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }
        .btn {
            border: 2px solid black;
            background-color: #f44336;
            color: black;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
        }
        /* Red */
        .danger {
            border-color: white;
            color: white;
        }

        .danger:hover {
            background: #f44336;
            color: white;
        }
        .closebtn:hover {
            color: black;
        }
    </style>

    <div class="alert">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <h2>Keeping ahead of COVID-19</h2>
        This rapidly evolving coronavirus  (COVID-19) has imposed an unsettling, fluid situation upon our community and its businesses. While the team here at
        Burlington Hyundai  still aims to maintain a "business as usual" approach, we are making a number of significant changes to our operations to account for a
        situation that is far from normal<br><br>
        <a href="https://demo.cromwells.co.uk/frontend/JuneCovid/information">
            <button class="btn danger">More Information</button>
        </a>
    </div>
    </body>
    </html>
    {*********** END OF CORONAVIRUS BANNER **************}
    {$smarty.block.parent}
    {/block}


