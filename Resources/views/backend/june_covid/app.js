Ext.define('Shopware.apps.JuneCovid', {
    extend: 'Enlight.app.SubApplication',

    name:'Shopware.apps.JuneCovid',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [ 'Main' ],

    views: [
        'list.Window',
        'list.Covid',

        'detail.Covid',
        'detail.Window'
    ],

    models: [ 'Covid' ],
    stores: [ 'Covid' ],

    launch: function() {
        return this.getController('Main').mainWindow;
    }
});
