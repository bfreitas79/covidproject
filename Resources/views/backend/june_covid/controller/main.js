//The main controller is responsible for starting the application
// by creating and displaying the listing window:

Ext.define('Shopware.apps.JuneCovid.controller.Main', {
    extend: 'Enlight.app.Controller',

    init: function() {
        var me = this;
        me.mainWindow = me.getView('list.Window').create({ }).show();
    }
});
