
Ext.define('Shopware.apps.JuneCovid.store.Covid', {
    extend:'Shopware.store.Listing',

    configure: function() {
        return {
            controller: 'JuneCovid'
        };
    },

    model: 'Shopware.apps.JuneCovid.model.Covid'
});