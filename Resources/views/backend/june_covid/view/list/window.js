Ext.define('Shopware.apps.JuneCovid.view.list.Window', {
    extend: 'Shopware.window.Listing',
    alias: 'widget.covid-list-window',
    height: 450,
    title : '{s name=window_title}Banner listing{/s}',

    configure: function() {
        return {
            listingGrid: 'Shopware.apps.JuneCovid.view.list.Covid',
            listingStore: 'Shopware.apps.JuneCovid.store.Covid'
        };
    }
});
