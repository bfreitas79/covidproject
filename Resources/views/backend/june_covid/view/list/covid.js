Ext.define('Shopware.apps.JuneCovid.view.list.Covid', {
    extend: 'Shopware.grid.Panel',
    alias:  'widget.covid-listing-grid',
    region: 'center',

    configure: function() {
        return {
            detailWindow: 'Shopware.apps.JuneCovid.view.detail.Window'
        };
    }
});
