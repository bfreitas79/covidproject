//The only requirement for this component is that you have to provide
// a property named "record", which contains a Shopware.data.Model instance,
// while instancing the component. The detail window will be assembled
// automatically, based on the record.

Ext.define('Shopware.apps.JuneCovid.view.detail.Window', {
    extend: 'Shopware.window.Detail',
    alias: 'widget.covid-detail-window',
    title : '{s name=title}Lists of Banners{/s}',
    height: 420,
    width: 900
});