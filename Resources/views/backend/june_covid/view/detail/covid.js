Ext.define('Shopware.apps.JuneCovid.view.detail.Covid', {
    extend: 'Shopware.model.Container',
    padding: 20,

    configure: function() {
        return {
            controller: 'JuneCovid'
        };
    }
});
