
Ext.define('Shopware.apps.JuneCovid.model.Covid', {
    extend: 'Shopware.data.Model',

    configure: function() {
        return {
            controller: 'JuneCovid',
            detail: 'Shopware.apps.JuneCovid.view.detail.Covid'
        };
    },


    fields: [
        { name : 'id', type: 'int', useNull: true },
        { name : 'name', type: 'string' },
        { name : 'headline', type: 'string' },
        { name : 'content', type: 'string' },
        { name : 'bgcolor', type: 'string' },
        { name : 'bgimg', type: 'string' },
        { name : 'buttontext', type: 'string' },
        { name : 'buttonlink', type: 'string' }
    ]
});
